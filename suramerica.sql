-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 13-04-2015 a las 08:04:15
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `suramerica`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `itemslider`
--

CREATE TABLE IF NOT EXISTS `itemslider` (
`iditemslider` int(10) unsigned NOT NULL,
  `orden` int(10) unsigned NOT NULL,
  `titulo` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `url` text COLLATE utf8_spanish_ci NOT NULL,
  `idusuario` int(10) unsigned NOT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `itemslider`
--

INSERT INTO `itemslider` (`iditemslider`, `orden`, `titulo`, `descripcion`, `url`, `idusuario`, `estado`) VALUES
(8, 1, 'LOGÍSTICA DE SALUD', 'Brindamos el más cuidadoso y especializado servicio de Logística en Salud', '10ac878abd9328eb7375e772b9f64a5c.JPG', 1, 'A'),
(9, 1, 'Título 2', 'Lorem ipsum dolor sit amet consectetur adopscing elit peque ', '5378e1dfd819b7b813f29533961c1bfa.png', 1, 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
`idusuario` int(10) unsigned NOT NULL,
  `nombre` text COLLATE utf8_spanish_ci NOT NULL,
  `apellido` text COLLATE utf8_spanish_ci NOT NULL,
  `usuario` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `pass` char(32) COLLATE utf8_spanish_ci NOT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `nombre`, `apellido`, `usuario`, `pass`, `estado`) VALUES
(1, 'Edder', 'Sánchez', 'esanchez', 'e10adc3949ba59abbe56e057f20f883e', 'A'),
(2, 'Suramerica', 'Express Cargo', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'A');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `itemslider`
--
ALTER TABLE `itemslider`
 ADD PRIMARY KEY (`iditemslider`), ADD KEY `idusuario` (`idusuario`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
 ADD PRIMARY KEY (`idusuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `itemslider`
--
ALTER TABLE `itemslider`
MODIFY `iditemslider` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
MODIFY `idusuario` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `itemslider`
--
ALTER TABLE `itemslider`
ADD CONSTRAINT `itemslider_ibfk_1` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`idusuario`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
