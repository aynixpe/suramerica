<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|

*/
define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

/*
/--------------------------------------------------------------------------
/ Tablas de permisos
/--------------------------------------------------------------------------

define("SERVICIOS", 0);
define("OPERACIONES", 1);
define("CLIENTES", 2);
define("USUARIOS", 3);
define("CUENTAS", 4);
define("COMPROBANTES", 5);


/*
/--------------------------------------------------------------------------
/ ACCIONES DE PERMISOS
/--------------------------------------------------------------------------

define("CONSULTAR", 1);
define("AGREGAR", 2);
define("EDITAR", 4);
define("ELIMINAR", 8);

/*
/--------------------------------------------------------------------------
/ ESTADO
/--------------------------------------------------------------------------
*/
define("ACTIVO", 1);
define("INACTIVO", 2);

/*
/--------------------------------------------------------------------------
/ CODIGOS DE ERROR DE LA APLICACION
/--------------------------------------------------------------------------
*/
define("ERR_PERMISO", serialize(array("code" => 0, "message" => "No tiene permisos para acceder a este recurso")));
define("ERR_CONEXION", serialize(array("code" => 1, "message" => "Lo sentimos, la información no se ha podido registrar, inténte nuevamente en unos minutos o actualice la página")));

/* End of file constants.php */
/* Location: ./application/config/constants.php */
