<?php
class Item_slider_model extends CI_Model{
  function __construct(){
    parent::__construct();
    $this->load->database();
  }

  function insert($orden, $titulo, $descripcion, $url, $idusuario, $estado){
    $data = array(
      "orden" => $orden,
      "titulo" => $titulo,
      "descripcion" => $descripcion,
      "url" => $url,
      "idusuario" => $idusuario,
      "estado" => $estado
    );
    $this->db->trans_begin();
    $this->db->insert('itemslider', $data);
    $this->db->trans_complete();
    return $this->db->trans_status();
  }

  function update($iditemslider, $orden, $titulo, $descripcion, $url, $idusuario, $estado){
    $data = array(
      "orden" => $orden,
      "titulo" => $titulo,
      "descripcion" => $descripcion,
      "url" => $url,
      "idusuario" => $idusuario,
      "estado" => $estado
    );
    $this->db->where('iditemslider', $iditemslider);
    $this->db->trans_begin();
    $this->db->update('itemslider', $data);
    $this->db->trans_complete();
    return $this->db->trans_status();
  }

  function get_all($estado){
    $where = "iditemslider > -1";
    if( ($estado & ACTIVO) == ACTIVO )
        $where = " estado = 'A'";
    else if( ($estado & INACTIVO) == INACTIVO)
        $where = " estado = 'I'";
    $this->db->select('*');
    $this->db->from('itemslider');
    $this->db->where($where);
    $result = $this->db->get();
    return $result->result_array();
  }

  function get($idItemSlider, $estado){
    $where = "1=1";
    if( ($estado & ACTIVO) == ACTIVO )
        $where = " estado = 'A'";
    else if( ($estado & INACTIVO) == INACTIVO)
        $where = " estado = 'I'";
    $this->db->select('*');
    $this->db->from('itemslider');
    $this->db->where($where);
    $this->db->where('iditemslider', $idItemSlider);
    $result = $this->db->get();
    return $result->result_array();
  }

  public function update_estado_item($idItemSlider, $estado){
    $data = array(
      "estado" => ($estado == 'true') ? 'A' : 'I'
    );
    $this->db->where('iditemslider', $idItemSlider);
    $this->db->trans_begin();
    $this->db->update('itemslider', $data);
    $this->db->trans_complete();
    return $this->db->trans_status();
  }

  public function update_item($titulo, $descripcion, $url, $idItemSlider){
    $data = array(
      "titulo" => $titulo,
      "descripcion" => $descripcion,
    );
    if($url != "")
      $data['url'] = $url;
    $this->db->where('iditemslider', $idItemSlider);
    $this->db->trans_begin();
    $this->db->update('itemslider', $data);
    $this->db->trans_complete();
    return $this->db->trans_status();
  }

  public function delete_item($idItemSlider){
    $this->db->where('iditemslider', $idItemSlider);
    $this->db->trans_begin();
    $this->db->delete('itemslider');
    $this->db->trans_complete();
    return $this->db->trans_status();
  }
}
?>
