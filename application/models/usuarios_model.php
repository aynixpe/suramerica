<?php
class Usuarios_model extends CI_Model{
  public function __construct(){
    parent::__construct();
    $this->load->database();
  }

  public function login($usuario, $password){
    $this->db->select('*');
    $this->db->from('usuario');
    $this->db->where('usuario', $usuario);
    $this->db->where('pass', $password);
    $query = $this->db->get();
    if($query->num_rows() == 1){
        return $query->row();
    }
    return false;
  }

  public function get_usuario($idUsuario){
    $this->db->select('idusuario, nombre, apellido, usuario, estado');
    $this->db->from('usuario');
    $this->db->where('idusuario', $idUsuario);
    $result = $this->db->get();

    return $result->result_array();
  }

public function get_usuarios(){
    $this->db->select('idusuario, nombre, apellido, usuario, estado');
    $this->db->from('usuario');
    $result = $this->db->get();

    return $result->result_array();
  }

  public function insert($nombre, $apellido, $usuario, $pass){
    $data = array(
        "nombre" => $nombre,
        "apellido" => $apellido,
        "usuario" => $usuario,
        "pass" => $password,
        "estado" => 'A'
    );
    $this->db->trans_begin();
    $this->db->insert('usuario', $data);
    $this->db->trans_complete();

    return $this->db->trans_status();
  }

  public function update($idUsuario, $nombre, $apellido, $usuario){
    $data = array(
        "nombre" => $nombre,
        "apellido" => $apellido,
        "usuario" => $usuario,
    );
    $this->db->where('idusuario', $idUsuario);
    $this->db->trans_begin();
    $this->db->update('usuario', $data);
    $this->db->trans_complete();

    return $this->db->trans_status();
  }
}
?>
