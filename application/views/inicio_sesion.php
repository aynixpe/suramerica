<!DOCTYPE html>
<html lang='es'>
<head>
	<meta charset='utf-8'/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Suramerica Express Cargo</title>
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/bootstrap.min.css' />
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/font-awesome.min.css' />
	<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/main.css' />
</head>
<body>
    <header class='container-fluid'>
        <?php $this->load->view('header'); ?>
    </header>
    <section class='container-fluid'>
        <form class='form-signin' id='frmInicioSesion'>
            <h2 class='form-signin-heading blanco'>Ingresa tus datos</h2>
            <label for='txtUsuario' class='sr-only'>Usuario</label>
            <input type='text' id='txtUsuario' class='form-control input-top' placeholder='Usuario' required autofocus />
            <label for='txtPassword' class='sr-only'>Contraseña</label>
            <input type='password' id='txtPassword' class='form-control input-bottom' placeholder='Contraseña' required />
            <br>
            <button class='btn btn-lg btn-primary btn-block' type='submit'>Ingresar</button>
        </form>
    </section>
    <script type="text/javascript" src='<?php echo base_url(); ?>js/jquery-1.11.1.min.js'></script>
    <script type="text/javascript" src='<?php echo base_url(); ?>js/bootstrap.min.js'></script>
    <script type='text/javascript'>
        $(document).ready(function(e){
            $('#frmInicioSesion').on('submit', function(e){
                e.preventDefault();
                var usuario = $('#txtUsuario').val();
                var password = $('#txtPassword').val();

                $.ajax({
                    url:'<?php echo base_url();?>admin/login',
                    type:'post',
                    dataType:'json',
                    data:{
                        usuario: usuario,
                        password: password
                    },
                    success: function(result){
                        if(result)
                            location.href = '<?php echo base_url();?>admin';
                    }
                });
            });
        });
    </script>
</body>
</html>
