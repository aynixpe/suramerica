<!DOCTYPE html>
<html lang='es'>
<head>
	<meta charset='utf-8'/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Suramerica Express Cargo</title>
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/bootstrap.min.css' />
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/font-awesome.min.css' />
	<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/main.css' />
</head>
<body>
	<header>
		<?php $this->load->view('header'); ?>
	</header>
    <section class='qs-seccion'>
		<div class='container-fluid' style='min-height: 75vh;'>
			<div class='row bg-blanco' id='qs-seccion1' role='tabpanel' style='min-height: 75vh; position: relative; overflow: hidden'>
				<img style='height: 100%; top:0; left: 0; position:absolute;'  src='<?php echo base_url();?>img/logo-mini-mitad.svg'/>
				<div class='col-xs-3 col-sm-2'>
					<ul class='nav nav-pills nav-stacked' role='tablist'>
						<li class='active'><a href="#qs-nosotros" aria-controls="nosotros" role="tab" data-toggle="tab">Nosotros</a></li>
						<li><a href="#qs-mision" aria-controls="mision" role="tab" data-toggle="tab">Misión</a></li>
						<li><a href="#qs-vision" aria-controls="vision" role="tab" data-toggle="tab">Visión</a></li>
					</ul>
				</div>
				<div class='col-xs-9 col-sm-10 tab-content' role='tab-content' style='overflow: hidden;'>
					<div id='qs-nosotros' class='tab-pane active' aria-labelledby='nosotros-tab' role='tabpanel'>
						<div class='container-fluid'>
							<div class='row'>
								<div class='col-sm-8'>
									<h2>Nosotros</h2>
									<p>Somos un grupo empresarial que ofrece soluciones logísticas integrales económicas a sus actividades de comercio exterior. Suramerica
										Express Cargo cuenta con el mejor equipo de talentos humanos de la industria que brinda a través de su Servicio de Agencia de Aduana,
										envíos de entrega rápida, Transporte, Almacenaje y Distribución, un servicio logístico integrado de manera profesional, personalizada,
										eficiente y económica, para sus importaciones y/o exportaciones.<br><br>
										Diseñamos a la medida de nuestros clientes la plataforma de servicios logísticos y los mantenemos debidamente informados sobre
										la situación de cada uno de sus despachos de importación o embarques de exportación, realizando las coordinaciones necesarias con el
										resto de empresas relacionadas en un régimen.</p>
								</div>
								<div class='col-sm-4'>
									Imagen
								</div>
							</div>
						</div>
					</div>
					<div id='qs-mision' class='tab-pane' aria-labelledby='mision-tab' role='tabpanel'>
						<div class='container-fluid'>
							<div class='row'>
								<div class='col-sm-8'>
									<h2>Misión</h2>
									<p>Satisfacer a cada cliente, generando confianza y desarrollando soluciones creativas e innovadoras otorgando así un servicio de alta calidad.</p>
								</div>
								<div class='col-sm-4'>
									Imagen
								</div>
							</div>
						</div>
					</div>
					<div id='qs-vision' class='tab-pane' aria-labelledby='vision-tab' role='tabpanel'>
						<div class='container-fluid'>
							<div class='row'>
								<div class='col-sm-8'>
									<h2>Visión</h2>
									<p>Ser reconocidos como una excelente opción de servicios logísticos a través de confianza y liderazgo, apoyado por los mejores talentos
										humanos que componen nuestro equipo en la industria.</p>
								</div>
								<div class='col-sm-4'>
									Imagen
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--div class='row bg-blanco' id='qs-seccion1' role='tabpanel'>
			<div class='col-sm-4'>Imagen</div>
			<div class='col-sm-8'>
				<h2>Nosotros</h2>
				<p>Somos un grupo empresarial que ofrece soluciones logísticas integrales económicas a sus actividades de comercio exterior. Suramerica
					Express Cargo cuenta con el mejor equipo de talentos humanos de la industria que brinda a través de su Servicio de Agencia de Aduana,
					envíos de entrega rápida, Transporte, Almacenaje y Distribución, un servicio logístico integrado de manera profesional, personalizada,
					eficiente y económica, para sus importaciones y/o exportaciones.<br><br>
					Diseñamos a la medida de nuestros clientes la plataforma de servicios logísticos y los mantenemos debidamente informados sobre
					la situación de cada uno de sus despachos de importación o embarques de exportación, realizando las coordinaciones necesarias con el
					resto de empresas relacionadas en un régimen.</p>
				<h2>Misión</h2>
				<p>Satisfacer a cada cliente, generando confianza y desarrollando soluciones creativas e innovadoras otorgando así un servicio de alta calidad.</p>
				<h2>Visión</h2>
				<p>Ser reconocidos como una excelente opción de servicios logísticos a través de confianza y liderazgo, apoyado por los mejores talentos
					humanos que componen nuestro equipo en la industria.</p>
			</div>
		</div-->
    </section>
    <footer class='container-fluid'>
        <?php $this->load->view('footer'); ?>
    </footer>
    <script type="text/javascript" src='<?php echo base_url(); ?>js/jquery-1.11.1.min.js'></script>
    <script type="text/javascript" src='<?php echo base_url(); ?>js/bootstrap.js'></script>
    <script type="text/javascript" src='<?php echo base_url(); ?>js/main.js'></script>
    <script type='text/javascript'>
        var base_url = '<?php echo base_url(); ?>';
    </script>
    </body>
    </html>
