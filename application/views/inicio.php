<!DOCTYPE html>
<html lang='es'>
<head>
	<meta charset='utf-8'/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Suramerica Express Cargo</title>
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/bootstrap.min.css' />
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/font-awesome.min.css' />
	<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/main.css' />
</head>
<body>
	<header>
		<?php $this->load->view('header'); ?>
	</header>
	<section class='container-fluid'>
		<div class='row'>
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators" id='lst-items-indicators'>
				</ol>
				<div class="carousel-inner" role="listbox" id='lst-items'>

				</div>
				<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
		<div class='row bg-blanco' id='seccion-1'>
			<div class='col-sm-4 azul'>
				<div class='container-fluid'>
					<div class='row'>
						<div class='col-xs-4 col-sm-12 col-md-4'>
							<div class='bg-azul circle-img center-block'>
								<img src='<?php echo base_url();?>img/img_1.png' class='img-responsive'/>
							</div>
						</div>
						<div class='col-xs-8 col-sm-12 col-md-8'>
							<div>
								<h2>LOGÍSTICA EN SALUD</h2>
								<p class='text-justify'>Brindamos un completo servicio de envío, distribución y almacenamiento de documentos, muestras y paquetes desde su origen hasta su destino</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class='col-sm-4 azul'>
				<div class='container-fluid'>
					<div class='row'>
						<div class='col-xs-4 col-sm-12 col-md-4'>
							<div class='bg-azul circle-img center-block'>
								<img src='<?php echo base_url();?>img/mundo.png' class='img-responsive'/>
							</div>
						</div>
						<div class='col-xs-8 col-sm-12 col-md-8'>
							<div>
								<h2>COURIER INTERNACIONAL</h2>
								<p>Lorem ipsum dolor et sum bla bla En un lugar llamado la Mancha, Vivía un hidalgo caballero de adarga espada o de lanza no sé.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class='col-sm-4 azul'>
				<div class='container-fluid'>
					<div class='row'>
						<div class='col-xs-4 col-sm-12 col-md-4'>
							<div class='bg-azul circle-img center-block'>
								<img src='<?php echo base_url();?>img/img_1.png' class='img-responsive'/>
							</div>
						</div>
						<div class='col-xs-8 col-sm-12 col-md-8'>
							<div>
								<h2>LOGÍSTICA GENERAL</h2>
								<p>Lorem ipsum dolor et sum bla bla En un lugar llamado la Mancha, Vivía un hidalgo caballero de adarga espada o de lanza no sé.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class='row' id='seccion-2'>
			<div class='col-sm-7'>
				<div class='container-fluid'>
					<div class='row vertical-align'>
						<div class='col-xs-3'>
							<div style='max-width: 100%;'>
								<img src='<?php echo base_url();?>img/carrito.svg' class='img-responsive'/>
							</div>
						</div>
						<div class='col-xs-9'>
							<div class='blanco'>
								<h2>COMPRA POR INTERNET</h2>
								<p>Somos una empresa que facilita tus compras por internet.<br>Afíliate y tendrás una dirección virtual que será tu shipping Address en USA.
								Recibimos tus compras y las entregamos en tu domicilio.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class='col-sm-5 blanco'>
				<h3>Consúltanos</h3>
				<div class="input-group input-group-lg">
					<input type="text" class="form-control" placeholder="Escribe tu correo aquí">
					<span class="input-group-btn">
						<button class="btn btn-success" type="button">Aceptar</button>
					</span>
				</div>
			</div>
		</div>
		<div class='row bg-gris' id='seccion-3'>
			<div class='col-sm-3'>
				<img src='<?php echo base_url();?>img/logo_blanco.svg' class='img-responsive'>
			</div>
			<div class='col-sm-3 text-center blanco'>
				<h3>SERVICIOS</h3>
				<p>Lorem ipsum dolor sit amet consectetur adopscing elit peque eratLorem ipsum dolor sit amet consectetur adopscing elit peque erat</p>
			</div>
			<div class='col-sm-3 text-center blanco'>
				<h3>SEGURIDAD Y GARANTÍA</h3>
				<p>Lorem ipsum dolor sit amet consectetur adopscing elit peque eratLorem ipsum dolor sit amet consectetur adopscing elit peque erat</p>
			</div>
			<div class='col-sm-3 text-center blanco'>
				<h3>TE ASESORAMOS</h3>
					<i class='fa fa-phone fa-5x col-xs-3'></i>
				<p class='col-xs-9'>Lorem ipsum dolor sit amet consectetur adopscing elit peque eratLorem ipsum dolor sit amet consectetur adopscing elit peque erat</p>
			</div>
		</div>
	</section>
	<footer class='container-fluid'>
		<?php $this->load->view('footer'); ?>
	</footer>

	<script type="text/javascript" src='<?php echo base_url(); ?>js/jquery-1.11.1.min.js'></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>js/bootstrap.js'></script>
	<script type="text/javascript" src='<?php echo base_url(); ?>js/main.js'></script>
	<script type='text/javascript'>
		var base_url = '<?php echo base_url(); ?>';
		$(document).ready(function(e){
			get_items(function(data){
				var html = "";
				var indicators ="";
				var flag = 0;
				for(var i=0; i < data.length; i++){
					if(data[i].estado == "A"){
						indicators += "<li data-target='#carousel-example-generic' data-slide-to='" + flag + "' ";
						indicators += (flag == 0) ? "class='active'": "";
						indicators += "></li>";
						html += "<div class='item ";
						html += (flag == 0) ? " active" : "";
						html += "'>";
						html += "	<div class='contenedor'>";
						html += "		<img src='<?php echo base_url();?>files/redim/" + data[i].url + "' alt='" + data[i].titulo + "' class='img-responsive img-carousel'>";
						html += "		<div class='cuadrado'></div>";
						html += "		<div class='triangulo-bottom-left hidden-sm hidden-xs'></div>";
						html += "		<img class='logo-mini hidden-xs hidden-sm' src='<?php echo base_url();?>img/logo-mini.svg'/>";
						html += "		<div class='carousel-caption'>";
						html += "			<h2>" + data[i].titulo + "</h2>";
						html += "			<p>" + data[i].descripcion + "</p>";
						html += "		</div>";
						html += "	</div>";
						html += "</div>";
						flag++;
					}
				}
				$('#lst-items-indicators').html(indicators);
				$('#lst-items').html(html);
			});
		});
	</script>
</body>
</html>
