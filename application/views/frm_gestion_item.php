<form class='form-horizontal' id='frm-gestion-item' enctype="multipart/form-data">
    <input type='hidden' id='fgi-id-item-slider' name='iditemslider' value='-1'>
    <input type='hidden' id='fgi-flag' name='flag' value='false' />
    <div class='form-group'>
        <label for='fgi-txttitulo' class='control-label col-xs-3'>Título</label>
        <div class='col-xs-9'>
            <input type='text' id='fgi-txttitulo' name='titulo' class='form-control input-sm' placeholder='Título' required/>
        </div>
    </div>
    <div class='form-group'>
        <label for='fgi-txtdescripcion' class='control-label col-xs-3'>Descripción</label>
        <div class='col-xs-9'>
            <textarea id='fgi-txtdescripcion' name='descripcion' class='form-control input-sm' row='2' required></textarea>
        </div>
    </div>
    <div class='form-group'>
        <label for='txtImagen' class='control-label col-xs-3'>Imagen</label>
        <div class='col-xs-9'>
            <div class="fileinput fileinput-new" id='fgi-fileinput' data-provides="fileinput">
                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                </div>
                <div>
                    <span class="btn btn-primary btn-file btn-sm">
                        <span class="fileinput-new">Buscar Imagen</span>
                        <span class="fileinput-exists">Cambiar</span>
                        <input type="file" id='fgi-fileImagen' name='imagen'>
                    </span>
                    <a href="#" class="btn btn-danger btn-sm fileinput-exists" data-dismiss="fileinput">Borrar</a>
                </div>
            </div>
        </div>
    </div>
    <div id='fgi-alert' class='col-xs-9 col-xs-offset-3'>

    </div>
    <div class='form-group'>
        <div class='col-xs-9 col-xs-offset-3'>
            <button type='button' id='fgi-btn-cancelar' class='btn btn-danger btn-sm'><i class='fa fa-times'></i> Cancelar </button>
            <button type='submit' class='btn btn-primary btn-sm'><i class='fa fa-save'></i> Aceptar </button>
        </div>
    </div>
</form>
