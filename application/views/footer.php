<div class='row footer'>
    <div class='col-sm-4'>
        Suramerica express cargo
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x "></i>
            <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
        </span>
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x "></i>
            <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
        </span>
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x "></i>
            <i class="fa fa-youtube fa-stack-1x fa-inverse"></i>
        </span>
        <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x "></i>
            <i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
        </span>
    </div>
    <div class='col-sm-5 vcenter'>
        <i class='fa fa-phone fa-2x'></i>&nbsp;&nbsp;
        <span class='text-left'>Te asesoramos (+51) 015294259 - 015399129</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <i class='fa fa-map-marker fa-2x'></i>&nbsp;&nbsp;
        <span class='text-left'>Ca. Federico Blume 134, Dpto 301 Urb. Naval Antares SMP - Lima</span>
    </div>
    <div class='col-sm-3'>
        Desarrollado por aynix business
    </div>
</div>
