<!DOCTYPE html>
<html lang='es'>
<head>
  <meta charset='utf-8'/>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Contáctanos - Suramerica Express Cargo</title>
  <link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/bootstrap.min.css' />
  <link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/font-awesome.min.css' />
  <link href='http://fonts.googleapis.com/css?family=Oswald:700,400,300' rel='stylesheet' type='text/css'>
  <link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/main.css' />
  <title></title>
</head>
<body>
  <header>
    <?php $this->load->view('header'); ?>
  </header>
  <section class='ct-section'>
    <div class="container-fluid" style='min-height: 80vh;'>
      <div class="row bg-blanco" style='min-height: 80vh; position: relative; overflow: hidden'>
        <div class="col-lg-5 contact">
          <h2 class='ct-titulo'>Contáctanos</h2>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3902.4804990359053!2d-77.08244560000003!3d-12.010405699999987!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105ce9262eb10e5%3A0x217f5b63bfa18512!2sFederico+Blume+134%2C+Lima+15108%2C+Per%C3%BA!5e0!3m2!1ses-419!2ses!4v1430256111078" frameborder="0" style="border:0"></iframe><br>
          <i class='fa fa-phone fa-2x ct-texto'></i>&nbsp;&nbsp;&nbsp;&nbsp;
          <span class='ct-texto'>(+51) 015294259 - 015399129</span><br>
          <i class='fa fa-map-marker fa-2x ct-texto'></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <span class='ct-texto'>Ca. Federico Blume 134, Dpto 301<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Urb. Naval Antares SMP - Lima</span><br>
          <i class='fa fa-envelope-o fa-2x ct-texto'></i>&nbsp;&nbsp;
          <span class='ct-texto'>a.ancasi@suramericaexpress.com.pe<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            operaciones@suramericaexpress.com.pe</span>
        </div>
        <div class="col-lg-7 formular">
            <form class='form-horizontal col-condensed' id='frm-contacto'>
              <div class='col-md-6'>
                  <div class='form-group'>
                      <label class='col-xs-3 col-md-2 control-label'>Nombre</label>
                      <div class='col-xs-9 col-md-10'>
                          <input type='text' id='txtNombre' class='form-control input-sm sin-radius' required/>
                      </div>
                  </div>
              </div>
              <div class='col-md-6'>
                  <div class='form-group'>
                      <label class='col-xs-3 col-md-2 control-label'>Correo</label>
                      <div class='col-xs-9 col-md-10'>
                          <input type='email' id='txtCorreo' class='form-control input-sm sin-radius' required/>
                      </div>
                  </div>
              </div>
              <div class='col-md-6'>
                  <div class='form-group'>
                      <label class='col-xs-3 col-md-2 control-label'>Empresa</label>
                      <div class='col-xs-9 col-md-10'>
                          <input type='text' id='txtEmpresa' class='form-control input-sm sin-radius' required/>
                      </div>
                  </div>
              </div>
              <div class='col-md-6'>
                  <div class='form-group'>
                      <label class='col-xs-3 col-md-2 control-label'>Teléfono</label>
                      <div class='col-xs-9 col-md-10'>
                          <input type='text' id='txtTelefono' class='form-control input-sm sin-radius'/>
                      </div>
                  </div>
              </div>
              <div class='col-sm-12'>
                  <div class='form-group'>
                      <label class='col-xs-3 col-md-1 control-label'>Consulta</label>
                      <div class='col-xs-9 col-md-11'>
                          <textarea class="form-control sin-radius" id='txtConsulta' rows="3" required></textarea>
                      </div>
                  </div>
              </div>
              <div class='text-center'>
                  <button type='submit' class='btn btn-lg btn-primary btn-contactar sin-radius'>Enviar</button>
              </div>
          </form>
        </div>
    </div>
  </section>
  <footer class='container-fluid'>
    <?php $this->load->view('footer'); ?>
  </footer>
  <script type="text/javascript" src='<?php echo base_url(); ?>js/jquery-1.11.1.min.js'></script>
  <script type="text/javascript" src='<?php echo base_url(); ?>js/bootstrap.js'></script>
  <script type="text/javascript" src='<?php echo base_url(); ?>js/main.js'></script>
  <script type='text/javascript'>
      var base_url = '<?php echo base_url(); ?>';
  </script>
</body>
</html>
