<!DOCTYPE html>
<html lang='es'>
  <head>
    <meta charset='utf-8'/>
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<title>Suramerica Express Cargo - Servicios</title>
  	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/bootstrap.min.css' />
  	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/font-awesome.min.css' />
  	<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
  	<link rel='stylesheet' type='text/css' href='<?php echo base_url(); ?>css/main.css' />
  </head>
  <body>
    <header>
      <?php $this->load->view('header'); ?>
    </header>
    <section class='qs-seccion'>
		<div class='container-fluid' style='min-height: 75vh;'>
			<div class='row bg-blanco' id='qs-seccion1' role='tabpanel' style='min-height: 75vh; position: relative; overflow: hidden'>
				<img style='height: 100%; top:0; left: 0; position:absolute;'  src='<?php echo base_url();?>img/logo-mini-mitad.svg'/>
				<div class='col-xs-3 col-sm-2'>
					<ul class='nav nav-pills nav-stacked' role='tablist'>
						<li class='active'><a href="#s-servicio1" aria-controls="servicio1" role="tab" data-toggle="tab">Logística en Salud</a></li>
						<li><a href="#s-servicio2" aria-controls="servicio2" role="tab" data-toggle="tab">Logística Postal</a></li>
						<li><a href="#s-servicio3" aria-controls="servicio3" role="tab" data-toggle="tab">Servicio 3</a></li>
            <li><a href="#s-servicio4" aria-controls="servicio4" role="tab" data-toggle="tab">Servicio 4</a></li>
					</ul>
				</div>
				<div class='col-xs-9 col-sm-10 tab-content' role='tab-content' style='overflow: hidden;'>
					<div id='s-servicio1' class='tab-pane active' aria-labelledby='servicio1-tab' role='tabpanel'>
						<div class='container-fluid'>
							<div class='row'>
								<div class='col-sm-8'>
									<h2>Logística en Salud</h2>
									<p>Brindamos el más cuidadoso y especializado servicio de Logística en Salud; que incluye exportación, importación,
                                    distribución, almacenamiento y acondicionamiento de:</p>
                                    <ul>
                                        <li>Especímenes de diagnóstico</li>
                                        <li>Medicamentos y Vacunas</li>
                                        <li>Drogas experimentales</li>
                                        <li>Productos controlados</li>
                                        <li>Sustancias peligrosas</li>
                                        <li>Insumos médicos</li>
                                    </ul>
                                    <p>
                                        Todas nuestras operaciones cumplen con las normas internacionales de transporte, de
                                        acuerdo con las reglamentaciones vigentes en los países de origen y destino, y se
                                        desarrollan siempre bajo el control de nuestra Dirección Global Técnica.
                                    </p>
								</div>
								<div class='col-sm-4'>
									Imagen
								</div>
							</div>
						</div>
					</div>
					<div id='s-servicio2' class='tab-pane' aria-labelledby='servicio2-tab' role='tabpanel'>
						<div class='container-fluid'>
							<div class='row'>
								<div class='col-sm-8'>
									<h2>Logística Postal</h2>
									<p>Descripción</p>
								</div>
								<div class='col-sm-4'>
									Imagen
								</div>
							</div>
						</div>
					</div>
					<div id='s-servicio3' class='tab-pane' aria-labelledby='servicio3-tab' role='tabpanel'>
						<div class='container-fluid'>
							<div class='row'>
								<div class='col-sm-8'>
									<h2>Servicio 3</h2>
									<p>Hacemos el Servicio 3.</p>
								</div>
								<div class='col-sm-4'>
									Imagen
								</div>
							</div>
						</div>
					</div>
          <div id='s-servicio4' class='tab-pane' aria-labelledby='servicio4-tab' role='tabpanel'>
						<div class='container-fluid'>
							<div class='row'>
								<div class='col-sm-8'>
									<h2>Servicio 4</h2>
									<p>Hacemos el Servicio 4.</p>
								</div>
								<div class='col-sm-4'>
									Imagen
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </section>
    <footer class='container-fluid'>
      <?php $this->load->view('footer'); ?>
    </footer>
    <script type="text/javascript" src='<?php echo base_url(); ?>js/jquery-1.11.1.min.js'></script>
    <script type="text/javascript" src='<?php echo base_url(); ?>js/bootstrap.js'></script>
    <script type="text/javascript" src='<?php echo base_url(); ?>js/main.js'></script>
    <script type='text/javascript'>
        var base_url = '<?php echo base_url(); ?>';
    </script>
  </body>
</html>
