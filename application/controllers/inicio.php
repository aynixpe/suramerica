<?php
class Inicio extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
  }

	public function index(){
		$this->load->view('inicio');
	}

  public function quienes_somos(){
    $this->load->view('quienes_somos');
  }

  public function servicios(){
    $this->load->view('servicios');
  }

  public function contactanos(){
    $this->load->view('contactanos');
  }
}
?>
